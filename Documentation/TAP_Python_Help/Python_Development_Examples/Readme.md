# Getting Started
The best way to get started is to explore the example plugins. 

The development examples are found in the PluginExample folder, which is included in the installation. The folder contains a number of Python files that shows how to use many of the OpenTAP features that you will need to develop you own plugins.
The process depends a bit on which platform you are on, please follow one of the following:
